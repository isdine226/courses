

select *
from omop.person 
where sex = 'M' and birth_date > '01/01/2020';

-- On affiche les colonnes des deux tables
select *
from visit_occurrence vo, person p
where p.person_id = vo.person_id
and sex = 'M';

-- On affiche seulement les colonnes de visit_occurrence 
select vo.*
from visit_occurrence vo, person p
where p.person_id = vo.person_id
and sex = 'M';

-- On affiche seulement deux champs de visit_occurrence
select vo.visit_occurrence_id, vo.start_date
from visit_occurrence vo, person p
where p.person_id = vo.person_id
and sex = 'M';

select vo.*
from visit_occurrence vo
inner join person p
on p.person_id = vo.person_id
where sex = 'M';


select sex, count(*) as nb
from person p, visit_occurrence vo 
where p.person_id = vo.person_id
group by sex;

select vo.*
from omop.visit_occurrence vo, omop.person p 
where vo.person_id = p.person_id 
and vo.visit_start_date > '01/01/2020'
and vo.visit_end_date < '05/05/2020'
and p.sex = 'F'
order by vo.visit_start_date;

select vo.*
from care_site cs, visit_occurrence vd, visit_detail vd
where vd.visit_occurrence_id = vd.visit_occurrence_id
and vd.care_site_id = vo.care_site_id
and care_site_code = 'UF40';
