--------------------------------------------------------------
--
--------------------------------------------------------------

create schema test; 

drop table if exists test.concept_1000;
create table if not exists test.concept_1000 as select * from omop.concept FETCH FIRST 1000 ROWS only;

drop table if exists test.concept_1000000;
create table test.concept_1000000 as select * from omop.concept FETCH FIRST 1000000 ROWS only;

explain
select * from test.concept_1000 where concept_code = '2';

explain
select * from test.concept_1000000 where concept_code = '2';

explain 
select * from test.concept_1000000 where concept_code = '2' 
order by concept_name;

explain 
select * from test.concept_1000 t1 inner join test.concept_1000000 t2
on t1.concept_id = t2.concept_id;

ALTER TABLE test.concept_1000 ADD CONSTRAINT xpk_concept_1 PRIMARY KEY (concept_id);
ALTER TABLE test.concept_1000000 ADD CONSTRAINT xpk_concept_2 PRIMARY KEY (concept_id);

explain 
select * from test.concept_1000 t1 inner join test.concept_1000000 t2
on t1.concept_id = t2.concept_id;

CREATE INDEX idx_concept_id_1 ON test.concept_1000 (concept_id ASC);

CREATE INDEX idx_concept_id_2 ON test.concept_1000000 (concept_id ASC);

explain 
select * from test.concept_1000 t1 inner join test.concept_1000000 t2
on t1.concept_id = t2.concept_id;

CLUSTER test.concept_1000  USING idx_concept_id_1 ;
CLUSTER test.concept_1000000  USING idx_concept_id_2 ;

explain 
select * from test.concept_1000 t1 inner join test.concept_1000000 t2
on t1.concept_id = t2.concept_id;
---

-- Statistics collector
select * from pg_stat_activity;
select * from pg_stat_user_tables;

select * from log_executor_stats;

select * from pg_statio_user_tables;

select * from pg_stat_user_indexes;




explain
select * from omop.concept where concept_code = '2';

explain
select * from omop.concept where concept_code = '2';

DROP INDEX omop.idx_concept_code;

explain
select * from omop.concept where concept_code = '2';

CREATE INDEX idx_concept_code ON omop.concept (concept_code ASC);

select * from omop.concept where concept_id = 10200;

select * from pg_catalog.pg_index;

ALTER TABLE omop.concept ADD CONSTRAINT xpk_concept PRIMARY KEY (concept_id);


CREATE UNIQUE INDEX idx_concept_concept_id  ON omop.concept  (concept_id ASC);
CLUSTER omop.concept  USING idx_concept_concept_id ;

c





