

-- Insertion of records in PATIENT

-- One row

INSERT INTO `PATIENT`(`FIRST_NAME`, `LAST_NAME`, `BIRTH_DATE`, `SEX`) 
VALUES ('First name 1', 'Name 1', '2000-03-30', 'F');

-- Multiples rows

INSERT INTO `PATIENT`(`FIRST_NAME`, `LAST_NAME`, `BIRTH_DATE`, `SEX`) 
VALUES 
	('First name 2', 'Name 2', '1980-02-20', 'F'),
	('First name 3', 'Name 3', '1972-03-05', 'H'),
	('First name 4', 'Name 4', '1938-09-20', 'H'),
	('First name 5', 'Name 5', '1940-10-01', 'F'),
	('First name 6', 'Name 6', '1965-05-06', 'F'),
	('First name 7', 'Name 7', '1967-06-07', 'H'),
	('First name 8', 'Name 8', '1981-03-31', 'F'),
	('First name 9', 'Name 9', '2010-03-12', 'F'),
	('First name 10', 'Name 10', '2020-03-10', 'H');


INSERT INTO `PATIENT`(`FIRST_NAME`, `LAST_NAME`, `BIRTH_DATE`, `SEX`) 
VALUES 
	('First name 11', 'Name 11', '1981-02-20', 'F'),
	('First name 12', 'Name 12', '1973-03-05', 'H'),
	('First name 13', 'Name 13', '1939-09-20', 'H'),
	('First name 14', 'Name 14', '1941-10-01', 'F'),
	('First name 15', 'Name 15', '1967-05-06', 'F'),
	('First name 16', 'Name 16', '1969-06-07', 'H'),
	('First name 17', 'Name 17', '1984-03-31', 'F'),
	('First name 18', 'Name 18', '2011-03-12', 'F'),
	('First name 19', 'Name 19', '2009-03-10', 'H'),
	('First name 20', 'Name 20', '2099-01-01', 'H')
	;