
------------------------------------------------------------------ 
-- select
------------------------------------------------------------------ 

select * from patient;

select id_patient, sexe from patient;

select count(*) from patient;

-- Q1 Afficher la table patient 



-- where
------------------------------------------------------------------ 

select *
from patient 
where sexe = 'M';

select *
from patient
where date_naissance > '1960-01-01';

-- Q2 selectionner les patients de la ville 1

-- Q3 afficher les patients nés après le 31/03/1986

-- AND
------------------------------------------------------------------ 

select *
from patient
where date_naissance > '1960-01-01'
and sexe = 'F';

-- Q4 afficher les séjours commencer après le 01/02/2020 dans l'hopital 1


-- IN
------------------------------------------------------------------ 

select * 
from patient
where id_ville in (1, 2);


-- GROUP BY
------------------------------------------------------------------ 

select sexe, count(*)
from patient
group by sexe;

-- INNER JOIN
------------------------------------------------------------------ 

select *
from patient p inner join sejour s 
on p.id_patient = s.id_patient;

-- 



-- insert
------------------------------------------------------------------ 



-- update
------------------------------------------------------------------ 



-- delete
------------------------------------------------------------------ 