/***********************************************************
*
* MongoDB
*
* Antoine Lamer
*
* 26/01/22
*
***********************************************************/


/* Create a collection                 */
/***************************************/

/* Show collections in database  */
/*********************************/


/* Insert one document  */
/*************************/
db.patient.insertOne(
    { patient_id : 1, first_name: "first_name_1", last_name: "last_name_1", age: 30, sex: "H" }
)

db.patient.insertOne(
    {patient_id : 2, first_name: "first_name_2", last_name: "last_name_2", age: 45, height:170, weight:74 }
)

db.patient.insertOne(
    {patient_id : 3, first_name: "first_name_3", last_name: "last_name_3", age: 45, height:170, weight:74,
        address : { zipcode: "59000", city: "Lille" } 
        }
)

/* Insert many documents */
/*************************/

db.patient.insertMany([
    { patient_id : 4, first_name: "first_name_4", last_name: "last_name_3", age: 67, height:172, weight:68},
    { patient_id : 5, first_name: "first_name_5", last_name: "last_name_5", sex: "H", age: 34, height:181, weight:80,
        address : { zipcode: "59000", city: "Lille" } },
    { patient_id : 6, first_name: "first_name_6", last_name: "last_name_6", sex: "F", age: 36, height:178, weight:80,
        address : { zipcode: "59100", city: "Roubaix" } },
    { patient_id : 7, first_name: "first_name_7", last_name: "last_name_7", sex: "F", age: 32, height:171, weight:60,
        address : { zipcode: "59113", city: "Seclin" } },
    { patient_id : 8, first_name: "first_name_8", last_name: "last_name_8", sex: "H", age: 32, height:175, weight:69,
        address : { zipcode: "59200", city: "Tourcoing" } }
    ] )

db.patient.insertMany([
    { patient_id : 9, first_name: "first_name_9", last_name: "last_name_9", age: 67, height:172, weight:68},
    { patient_id : 10, first_name: "first_name_10", last_name: "last_name_10", sex: "F", age: 22, height:171, weight:67,
        address : { zipcode: "59000", city: "Lille" },
        dignosis_code : [1, 2, 5] },
    { patient_id : 11, first_name: "first_name_11", last_name: "last_name_11", sex: "F", age: 36, height:177, weight:84,
        address : { zipcode: "59000", city: "Lille" },
        dignosis_code : [2, 5]  },
    { patient_id : 12, first_name: "first_name_12", last_name: "last_name_12", sex: "F", age: 32, height:181, weight:70,
        address : { zipcode: "59113", city: "Seclin" },
        dignosis_code : [2, 3, 10]  },
    { patient_id : 13, first_name: "first_name_13", last_name: "last_name_13", sex: "H", age: 32, height:175, weight:69,
        address : { zipcode: "59200", city: "Tourcoing" },
        dignosis_code : [1, 2, 5]  },
    { patient_id : 14, first_name: "first_name_14", last_name: "last_name_14", sex: "F", age: 32, height:175, weight:74,
        address : { zipcode: "59200", city: "Tourcoing" },
        dignosis_code : [1, 2, 5]  },
    { patient_id : 15, first_name: "first_name_15", last_name: "last_name_15", sex: "H", age: 12, height:145, weight:49,
        address : { zipcode: "59200", city: "Tourcoing" },
        dignosis_code : [4, 5]  },      
    { patient_id : 16, first_name: "first_name_16", last_name: "last_name_16", sex: "H", age: 44, height:180, weight:72,
        address : { zipcode: "59200", city: "Tourcoing" },
        dignosis_code : [1, 4, 5]  },
    { patient_id : 17, first_name: "first_name_17", last_name: "last_name_17", sex: "H", age: 56, height:172, weight:75,
        address : { zipcode: "59200", city: "Tourcoing" },
        dignosis_code : [1, 2, 6]  },
    { patient_id : 18, first_name: "first_name_18", last_name: "last_name_18", sex: "F", age: 76, height:175, weight:70,
        address : { zipcode: "59200", city: "Tourcoing" },
        dignosis_code : [1, 3, 5]  },
    { patient_id : 19, first_name: "first_name_19", last_name: "last_name_19", sex: "H", age: 32, height:175, weight:65,
        address : { zipcode: "59200", city: "Tourcoing" },
        dignosis_code : [2, 3, 5] }, 
    { patient_id : 20, first_name: "first_name_20", last_name: "last_name_20", sex: "H", age: 32, height:175, weight:62,
        address : { zipcode: "59200", city: "Tourcoing" },
        dignosis_code : [4, 5]  },
    ] )

/* Insert patient 21 : age = 30, sex = H, height = 180, weight = 70, bmi = 21,60
/************************************/


/* Select with conditions */
/**************************/  

/* select all patients with sex = F */



/* select all patients with age > 50 */


/* select all patients with sex = H and age < 50 */


/* select all patients and sort them by decreasing age */


/* select all patients who did not have the field 'sex' */



/* Update one patient 1, set age = 29 */
/************************************/


/* Update patient 21, delete the field bmi */
/************************************/


/* Update patient 2, increase age by 1 */
/************************************/


/* Update all patients by increasing the age by 1 /
/*************************/  
    

/* Update all patients with age with an age above 50 by increasing the age by 1 /
/*************************/  

    
/* Agregation            */
/*************************/

// Count the patients


// Count the patients with an address

// Count the patients with age > 60

// Count the patients with age > 60 and sex = male


// Count patients by sex modalities (by filtering on patients whose sex is documented)


// Count the patients per city and compute the min/age age par city


// Count the patients per city and compute the min/age age par city and order by number of patients 

/* MapReduce            */
/*************************/

// Count patients by city using map reduce
