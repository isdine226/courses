#########################################
#
# Exercice : Manipulation des fichiers INSEE
# Objetifs :
# - data management
#
# Date de création : kddkd
# Date de dernière modification :
# Auteur : 
#
#########################################

## Chargement des libraries
library(dplyr)

## Chemins
path_import = '/home/ant/Documents/00_Cours/courses/R/data'
path_export = '/home/ant/Documents/00_Cours/courses/R/data/export'

## Fonctions
taux = function(numerateur, denominateur, precision = 2){
return(round(numerateur / denominateur * 100, precision))
}

# -----------------------------------

# Chemin et fichiers disponibles
list.files(path_import)

# 1.1) Chargement des données
##########################################################################

## Fichier Population
age_pop_raw = read.csv2(file.path(path_import, 'BTT_TD_POP1B_2017.csv'), dec = '.')

## Fichier décès


## Immigration


## Statuts professionnels


# 1.2) Exploration des fichiers
# str / head


# 2) Nettoyage données
#########################################################################

# Population insee
# -----------------------------------------------------------------------

# Noms de colonnes en minuscules
colnames(age_pop_raw) = tolower(colnames(age_pop_raw))

# Ajout colonne département
# Somme de l'âge (age * nombre)
# Filtre département vide
# Indicateurs (ex : nombre d'habitants par département)
age_pop_departement = age_pop_raw %>%
  dplyr::mutate(departement = substr(codgeo, 1, 2),
                 somme_age = aged100 * nb) %>%
  dplyr::filter(departement != "") %>%
  dplyr::group_by(departement) %>%
  dplyr::summarise(
    nb_habitants = round(sum(nb,na.rm=TRUE)),
    nb_hommes = round(sum(nb[sexe == 1], na.rm = TRUE)),
    nb_femmes = round(sum(nb[sexe == 2], na.rm = TRUE)),
    somme_age = round(sum(somme_age, na.rm = TRUE)),
    ) %>%
  mutate(taux_hommes_vivants = taux(nb_hommes, nb_hommes + nb_femmes),
         age_moyen_vivant = round((somme_age / nb_habitants), 2))

# Décès INSEE
# -----------------------------------------------------------------------

# Calculer l'âge moyen par département


# Immmigration
# -----------------------------------------------------------------------

# Calculer le taux d'immigration


# Status pro
# -----------------------------------------------------------------------

colnames(csp_raw) = c('departement', 'lib_departement', 'taux_agriculteurs_exploitants', 'taux_artisans_commerçants_chef_entreprises', 'taux_cadres', 'taux_profs_intermediaires', 'taux_employés', 'taux_ouvriers', 'taux_retraités', 'taux_sans_act_pro')
head(csp_raw)
csp_departement = csp_raw[4:nrow(csp_raw),]

cols =  colnames(csp_departement)[!colnames(csp_departement) %in% c('departement', 'lib_departement')]
csp_departement[cols] = lapply(csp_departement[cols], as.numeric)

csp_departement = csp_departement %>%
  select(-lib_departement)


# Merge
# -----------------------------------------------------------------------

pop = age_pop_departement %>%
  # Décès
  merge(deces_departement, by = 'departement') %>%
  #
  merge(img_departement, by = 'departement') %>%
  #
  merge(csp_departement, by = 'departement') %>%
  #
  merge(cs, by = 'departement')

# Save the final dataset
# -----------------------------------------------------------------------

pop %>%
  write.csv2(file.path(path_export, 'pop.csv'), row.names = FALSE)
