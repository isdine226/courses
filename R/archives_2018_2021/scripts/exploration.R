#########################################
#
# Exercice : Exploration du jeu de données
#
#########################################

## Chargement des libraries
library(dplyr)

## Chemins
path_import = '/home/ant/Documents/00_Cours/courses/R/data'
path_export = '/home/ant/Documents/00_Cours/courses/R/data/export'

list.files(path_import)

# 1.1) Chargement des données
##########################################################################

## Fichier Population
pop = read.csv2(file.path(path_export, 'pop.csv'), dec = ',')
str(pop)

# 1) Nombre d'habitants
#########################################################################

# Description du nombre d'habitants par département

# 2) Décès
#########################################################################

# Description du nombre de décès par département



pop %>% select(departement, age_moyen_deces) %>%
  arrange(age_moyen_deces) %>%
  head()

# 3) Immigration
#########################################################################



# 4) CSP
#########################################################################



# 5) 
#########################################################################

# age_moyen_deces ~ taux_sans_act_pro
# age_moyen_deces ~ taux_retraités

