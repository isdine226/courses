#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)
library(shinyWidgets)

library(ggplot2)
library(dplyr)

path = '/home/ant/Documents/00_Cours/courses/R/data/export'
data = read.csv2(file.path(path, 'deces_by_region.csv'))

liste_region = unique(data$region)

# Define UI for application that draws a histogram
ui = fluidPage(

    # Application title
    titlePanel("Exploration du jeu de données Mortalité par région / 2017"),

    # Sidebar with a slider input for number of bins 
    sidebarLayout(
        sidebarPanel(
            pickerInput(inputId = 'region',
                        label = 'Région',
                        choices = liste_region,
                        options = list(`style` = "btn-info")) 
        ),

        # Show a plot of the generated distribution
        mainPanel(
           plotOutput("mortalityPlot")
        )
    )
)

# Define server logic required to draw a histogram
server <- function(input, output) {

    output$mortalityPlot <- renderPlot({
            
        data %>%
            filter(region == input$region) %>%
            ggplot(aes(semaine_deces, prop_deces)) + geom_line()
    })
}

# Run the application 
shinyApp(ui = ui, server = server)
